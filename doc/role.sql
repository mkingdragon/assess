/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 8.0.19 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `role` (
	`role_id` int (11),
	`role_name` varchar (765),
	`user_name` varchar (765),
	`create_date` datetime ,
	`modify_date` datetime 
); 
insert into `role` (`role_id`, `role_name`, `user_name`, `create_date`, `modify_date`) values('1','管理员','Bilysen','2020-10-12 19:48:52','2020-10-17 15:17:09');
insert into `role` (`role_id`, `role_name`, `user_name`, `create_date`, `modify_date`) values('2','vip用户','CR7','2020-10-17 11:49:20','2020-10-17 15:20:51');
insert into `role` (`role_id`, `role_name`, `user_name`, `create_date`, `modify_date`) values('3','管理员','Bily哥','2020-10-21 20:37:57','2020-10-21 20:37:59');
insert into `role` (`role_id`, `role_name`, `user_name`, `create_date`, `modify_date`) values('4','vip用户','红魔','2020-10-21 17:43:25','2020-10-21 17:43:28');
insert into `role` (`role_id`, `role_name`, `user_name`, `create_date`, `modify_date`) values('5','普通用户','Hao','2020-10-21 20:32:59','2020-10-21 20:33:02');
insert into `role` (`role_id`, `role_name`, `user_name`, `create_date`, `modify_date`) values('6','vip用户','鲁尼','2020-10-21 20:39:18','2020-10-21 20:39:21');
insert into `role` (`role_id`, `role_name`, `user_name`, `create_date`, `modify_date`) values('9','vip用户','马努','2020-10-19 14:28:43','2020-10-19 14:28:43');
insert into `role` (`role_id`, `role_name`, `user_name`, `create_date`, `modify_date`) values('10','普通用户','Tom','2020-10-21 16:59:58','2020-10-21 16:59:58');
insert into `role` (`role_id`, `role_name`, `user_name`, `create_date`, `modify_date`) values('11','普通用户','小小Jack','2020-10-21 17:03:16','2020-10-21 22:07:03');
insert into `role` (`role_id`, `role_name`, `user_name`, `create_date`, `modify_date`) values('12','普通用户','Jack','2020-10-21 19:11:39','2020-10-21 19:11:39');
insert into `role` (`role_id`, `role_name`, `user_name`, `create_date`, `modify_date`) values('13','普通用户','Lin弟弟','2020-10-21 20:10:38','2020-10-21 20:10:38');
insert into `role` (`role_id`, `role_name`, `user_name`, `create_date`, `modify_date`) values('14','普通用户','锋神','2020-10-21 20:51:36','2020-10-21 20:51:36');
insert into `role` (`role_id`, `role_name`, `user_name`, `create_date`, `modify_date`) values('16','普通用户','必成神','2020-10-21 23:48:23','2020-10-21 23:50:03');
