/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`assess` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `assess`;

/*Table structure for table `exam_project` */

DROP TABLE IF EXISTS `exam_project`;

CREATE TABLE `exam_project` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `project_name` varchar(20) DEFAULT NULL COMMENT '项目名字',
  `project_duration` varchar(20) DEFAULT NULL COMMENT '项目时长',
  `project_principal` varchar(20) DEFAULT NULL COMMENT '项目负责人',
  `project_creattime` datetime DEFAULT NULL COMMENT '项目创建时间',
  `project_updatetime` datetime DEFAULT NULL COMMENT '项目修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `exam_project` */

insert  into `exam_project`(`id`,`project_name`,`project_duration`,`project_principal`,`project_creattime`,`project_updatetime`) values (1,'学生管理系统','20天','张三','2020-10-11 10:49:46',NULL),
(2,'小吃街管理系统','50天','秦安','2020-10-11 14:43:51','2020-10-11 22:09:57'),
(3,'酒店管理系统','80天','张鑫','2020-10-11 17:40:50',NULL),
(4,'宿舍管理系统','20天','肖潇','2020-10-11 22:09:11',NULL),
(5,'前台商城系统','100天','慕药','2020-10-12 13:00:16',NULL),
(21,'前台商城系统','100天','慕药','2020-10-12 13:02:53',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
