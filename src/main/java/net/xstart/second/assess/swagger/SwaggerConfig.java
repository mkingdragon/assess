package net.xstart.second.assess.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * @program: assess
 * @description: swagger配置类
 * @author: 上河图
 * @create: 2020-10-20 23:31
 */

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    //    swagger的实例对象-->Docket
    @Bean
    public Docket docket(Environment environment){

        //选择要显示swagger的运行环境
        Profiles profiles = Profiles.of("dev");
        boolean acceptsProfiles = environment.acceptsProfiles(profiles);

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("刘善城-项目表")
                .enable(acceptsProfiles)  //判断是否为true 来是否开启swagger
                .select()
                //RequestHandlerSelectors配置接口方式   basePackage扫描注解下的接口
                .apis(RequestHandlerSelectors.basePackage("net.xstart.second.assess.controller.Item"))
                .build();

    }

    //    api接口信息
    @Bean
    public ApiInfo apiInfo(){

        Contact contact = new Contact(null,null,null);
        return new ApiInfo("项目表api文档说明",
                "负责项目表的CRUD和模糊查询以及分页",
                "2.3.4",
                null,
                contact,
                "Apache 2.3.4",
                null,
                new ArrayList());
    }

    //User表的swagger实例对象
    @Bean
    public Docket docketUser(Environment environment){
        //选择swagger环境
        Profiles profiles = Profiles.of("dev");
        boolean flag = environment.acceptsProfiles(profiles);
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfoUser())
                .groupName("朱伟焕-用户表")
                //是否启动swagger，默认为true
                .enable(flag)
                .select()
                .apis(RequestHandlerSelectors.basePackage("net.xstart.second.assess.controller.user"))
                .build();
    }

    private ApiInfo apiInfoUser(){
        //作者信息
        Contact contact = new Contact(null, null, null);
        return new ApiInfo(
                "用户表的Swagger—API文档",
                "实现用户的CURD操作",
                "v3.0",
                "http://localhost:8080/swagger-ui.html?urls.primaryName=%E6%9C%B1%E4%BC%9F%E7%84%95-%E7%94%A8%E6%88%B7%E8%A1%A8",
                contact,
                null,
                null,
                new ArrayList<>()
        );
    }

    //Role表的swagger实例
    @Bean
    public Docket docketRole(Environment environment){

        //选择swagger在dev的运行环境中显示
        Profiles profiles = Profiles.of("dev");
        boolean acceptsProfiles = environment.acceptsProfiles(profiles);

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfoRole())
                .groupName("赵必成-角色表")
                .enable(acceptsProfiles)  //判断是否开启swagger ,true开启
                .select()
                //RequestHandlerSelectors配置接口方式   basePackage扫描注解下的接口
                .apis(RequestHandlerSelectors.basePackage("net.xstart.second.assess.controller.role"))
                .build();

    }

    //角色api接口信息
    @Bean
    public ApiInfo apiInfoRole(){

        Contact contact = new Contact(null,null,null);
        return new ApiInfo("角色表api文档",
                "负责角色表的增删改查和模糊查询",
                "v1.0",
                null,
                contact,
                null,
                null,
                new ArrayList());
    }

    @Bean
    public Docket docketWork(Environment environment){

        //选择要显示swagger的运行环境
        Profiles profiles = Profiles.of("dev");
        boolean acceptsProfiles = environment.acceptsProfiles(profiles);

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("林梓锋-职业表")
                .enable(acceptsProfiles)  //判断是否为true 来是否开启swagger
                .select()
                //RequestHandlerSelectors配置接口方式   basePackage扫描注解下的接口
                .apis(RequestHandlerSelectors.basePackage("net.xstart.second.assess.controller.work"))
                .build();

    }

    //    api接口信息
    @Bean
    public ApiInfo apiInfoWork(){

        Contact contact = new Contact(null,null,null);
        return new ApiInfo("项目表api文档说明",
                "负责项目表的CRUD和模糊查询以及分页",
                "2.3.4",
                null,
                contact,
                "Apache 2.3.4",
                null,
                new ArrayList());
    }
}
