package net.xstart.second.assess.mapper.Item;

import net.xstart.second.assess.pojo.Item.item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;





/**
 * @program: assess
 * @description: 项目表的持久层
 * @author: 上河图
 * @create: 2020-10-10 22:46
 */
@Repository
public interface ItemMapper extends PagingAndSortingRepository<item,Integer> {

    //通过主键id删除项目
    @Transactional
    @Modifying
    @Query("delete from item i where i.id=?1")
    int DeleteItemById(int id);

    //分页加模糊查询 前提是前端传入项目名
    @Query("select i from item i  where i.projectName like %?1%")
    Page<item> QueryAndLimit(String ProjectName, Pageable pageable);



}
