package net.xstart.second.assess.mapper.work;

import net.xstart.second.assess.pojo.Item.item;
import net.xstart.second.assess.pojo.work.Work;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * @description 职位Mapper接口
 * @Author Lin
 * @Date 2020/10/12 10:23
 */
@Repository
public interface WorkMapper extends JpaRepository<Work, Integer>, JpaSpecificationExecutor<Work> {

    //通过id删除操作
    @Transactional
    @Modifying
    @Query("delete from Work w where w.wid = ?1")
    int deleteWorkById(Integer id);

    //模糊查询并分页
    @Query("select w from Work w where w.work like %?1%")
    Page<item> QueryAndLimit(String work, Pageable pageable);
}
