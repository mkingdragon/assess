package net.xstart.second.assess.mapper.user;

import net.xstart.second.assess.pojo.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import javax.transaction.Transactional;

/**
 * @program: assess
 * @description: mapper层
 * @author: WH
 * @create: 2020-10-10 13:44
 */
public interface UserMapper extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {

    //通过uid删除用户
    @Transactional
    @Modifying
    @Query("delete from User u where u.uid = ?1")
    int deleteUserById(int uid);

    //通过关键字（模糊）查询并且分页
    @Query("from User u where u.username like %:username%")
    Page<User> findByUser(Pageable pageable, @Param("username") String username);

}
