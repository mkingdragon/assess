package net.xstart.second.assess.mapper.role;

import net.xstart.second.assess.pojo.Item.item;
import net.xstart.second.assess.pojo.role.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;


/**
 * @authoor Bilysen
 * @date 2020/10/17 - 10:19
 * Mapper接口
 * 继承：
 *  JpaRepository<实体类类型, 实体类中主键属性>  封装了基本CRUD操作
 *  JpaSpecificationExecutor<实体类类型>  封装了复杂查询
 *
 **/

public interface RoleMapper extends JpaRepository<Role, Integer>, JpaSpecificationExecutor<Role> {

    //通过id删除用户(相比于原生的方法，此方法有返回值，利于我们判断是否有删除成功)
    @Transactional  //添加事务支持
    @Modifying  //声明为更新操作(除了查询之外)
    @Query("delete from Role r where r.roleId = ?1")
    public int deleteRoleById(int id);
}
