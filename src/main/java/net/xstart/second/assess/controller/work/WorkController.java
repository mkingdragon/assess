package net.xstart.second.assess.controller.work;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.xstart.second.assess.pojo.work.Work;
import net.xstart.second.assess.service.work.WorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @description 职业控制层
 * @Author Lin
 * @Date 2020/10/12 10:24
 */

@RestController
@RequestMapping("/work")
@Api(tags = "职位表控制层")
public class WorkController {

    @Autowired
    WorkService workService;

    //添加操作
    @PostMapping("/add")
    @ApiOperation("添加职位")
    public Map<String, Object> addWork(@Valid @RequestBody Work work, BindingResult result) {
        Map<String, Object> map = new HashMap<>();
        if (result.hasErrors()) {
            String message = result.getFieldError().getDefaultMessage();
            map.put("msg", message);
        } else {
            workService.addWork(work);
            map.put("msg", "success");
        }
        return map;
    }

    //删除操作
    @PostMapping("/delete")
    @ApiOperation("删除职位")
    public Map<String, Object> deleteWork(@RequestParam(value = "id") Integer id) {
        Map<String, Object> map = new HashMap<>();
        if (workService.deleteWorkById(id) > 0) {
            map.put("msg", "success");
        } else {
            map.put("msg", "error");
        }
        return map;
    }

    //修改操作
    @PostMapping("/update")
    @ApiOperation("修改职位")
    public Map<String, Object> updateWork(@Valid @RequestBody Work work, BindingResult result) {
        Map<String, Object> map = new HashMap<>();
        if (result.hasErrors()) {
            String message = result.getFieldError().getDefaultMessage();
            map.put("msg", message);
        } else {
            workService.updateWork(work);
            map.put("msg", "success");
        }
        return map;
    }

    //查询操作：根据id查询职位
    @PostMapping("/queryById")
    @ApiOperation("根据id查职位")
    public Map<String, Object> queryWorkById(@RequestParam(value = "id") Integer id) {
        Map<String, Object> map = new HashMap<>();
        Optional<Work> work = workService.queryWorkById(id);
        map.put("data", work);
        return map;
    }

    //查询操作：查询所有职位
    @PostMapping("/queryAll")
    @ApiOperation("查询所有职位")
    public Map<String, Object> queryAll() {
        Map<String, Object> map = new HashMap<>();
        List<Work> workList = workService.queryAllWork();
        map.put("data", workList);
        return map;
    }

    //模糊查询并分页
    @PostMapping("/queryWorkLike")
    @ApiOperation("模糊查询并分页")
    public Map<String, Object> queryWorkLike(@RequestBody @RequestParam(value = "str") String str) {
        Map<String, Object> map = new HashMap<>();
        if (str == null) {
            List<Work> workList = workService.queryAllWork();
            map.put("workList", workList);
        }else if (workService.QueryAndLimit(str) == null){
            map.put("msg","没有查询到任何职业");
        }
        else {
            Page<Work> workPage = workService.QueryAndLimit(str);
            List<Work> workList = workPage.getContent();
            map.put("workList", workList);
            map.put("TotalElements", workPage.getTotalElements());
            map.put("TotalPages", workPage.getTotalPages());
        }
        return map;
    }

}
