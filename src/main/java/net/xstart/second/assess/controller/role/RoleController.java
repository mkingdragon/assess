package net.xstart.second.assess.controller.role;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.xstart.second.assess.pojo.role.Role;
import net.xstart.second.assess.service.role.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;

/**
 * @authoor Bilysen
 * @date 2020/10/17 - 10:41
 **/
@RestController
@RequestMapping("/role")
@Api(tags = "角色表控制层")
public class RoleController {
    //自动装配bean-roleService
    @Autowired
    private RoleService roleService;

    //查询全部的角色用户并分页显示
    @ApiOperation("查询全部的角色用户并分页显示")
    @PostMapping("/roleList")
    public HashMap<String,Object> queryAllRoles(@RequestParam(value = "cP") @ApiParam("显示的页面，默认为第一页")int currentPage){
        HashMap<String,Object> map = new HashMap<>();
        //默认查找的都为第1页
        if(currentPage<=0){
            currentPage=1;
        }
        Page<Role> roles = roleService.queryRoles(currentPage);
        map.put("roleList",roles.getContent());             //角色列表
        map.put("totalPages",roles.getTotalPages());        //全部页面
        map.put("currentPage",currentPage);                 //当前页面
        map.put("totalRoles",roles.getTotalElements());     //全部角色用户数量
        map.put("pageRoles",roles.getNumberOfElements());   //当前页面的全部角色用户数量
        return map;
    }

    //增加角色用户
    @PostMapping("/addRole")
    @ApiOperation("增加角色用户")
    //@Valid注解可以实现数据的验证
    //@RequestBody 解析json字符串
    public HashMap<String,Object> addRole(@Valid @RequestBody @ApiParam("角色对象role（日期，不需要传参，后台自动生成）")Role role, BindingResult result){
        HashMap<String,Object> map = new HashMap<>();
        //表单传回的字段验证，返回相应信息
        if(result.hasErrors()){
            //封装错误信息
            String msg = result.getFieldError().getDefaultMessage();
            map.put("msg",msg);
        }else{
            //调用service层的方法添加用户
            Role role1 = roleService.addRole(role);
            if(role1!=null){
                map.put("msg","添加成功");
            }else{
                map.put("msg","添加失败");
            }
        }
        return map;
    }

    //删除角色用户
    @ApiOperation("删除角色用户")
    @PostMapping("/deleteRole")
    public HashMap<String,Object> deleteRole(@RequestParam(value = "rid") @ApiParam("角色id")int roleId){
        HashMap<String,Object> map = new HashMap<>();
        int i = roleService.deleteRole(roleId); //调用service层的删除用户
        //判断是否删除成功
        if(i>0){
            map.put("msg","删除成功");
        }else{
            map.put("msg","删除失败");
        }
        return map;
    }

    //得到更新角色的数据
    @ApiOperation("得到更新角色的数据")
    @PostMapping("/getUpdateRole")
    public HashMap<String,Object> getUpdateList(@RequestParam(value = "rid") @ApiParam("角色id")int roleId){
        HashMap<String,Object> map = new HashMap<>();
        Role role = roleService.queryRoleById(roleId); //调用service层的根据id得到用户
        map.put("updateRole",role);
        return map;
    }

    //更新角色数据
    @ApiOperation("更新角色数据")
    @PostMapping("/updateRole")
    public HashMap<String,Object> updateRole(@Valid @RequestBody @ApiParam("角色对象role（日期，不需要传参，后台自动生成）")Role role, BindingResult result){
        HashMap<String,Object> map = new HashMap<>();
        if(result.hasErrors()) {
            //封装错误信息
            String msg = result.getFieldError().getDefaultMessage();
            map.put("msg", msg);
        }else {
            Role role1 = roleService.updateRole(role); //调用service层更新数据
            if (role1 != null) {
                map.put("msg", "修改成功");
            } else {
                map.put("msg", "修改失败");
            }
        }
        return map;
    }

    //模糊查询
    @ApiOperation("模糊查询并分页")
    @PostMapping("/queryByLike")
    public HashMap<String,Object> queryByLike(@RequestParam("rn") @ApiParam("角色名称")String roleName,@RequestParam("un") @ApiParam("用户名称") String userName,@RequestParam(value = "cP") @ApiParam("当前显示的页面，默认为第一页")int currentPage){
        HashMap<String,Object> map = new HashMap<>();
        //默认查找的都为第1页
        if(currentPage<=0){
            currentPage=1;
        }
        Page<Role> roles = roleService.queryLike(roleName, userName, currentPage);      //调用service层模糊查询
        map.put("roleList",roles.getContent());             //角色列表
        map.put("totalPages",roles.getTotalPages());        //全部页面
        map.put("currentPage",currentPage);                 //当前页面
        map.put("totalRoles",roles.getTotalElements());     //全部角色用户数量
        map.put("pageRoles",roles.getNumberOfElements());   //当前页面的全部角色用户数量
        return map;
    }
}
