package net.xstart.second.assess.controller.user;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.xstart.second.assess.pojo.user.User;
import net.xstart.second.assess.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Optional;


/**
 * @program: assess
 * @description: 控制层
 * @author: WH
 * @create: 2020-10-10 13:45
 */
@RestController
@RequestMapping("/user")
@Api(tags = "用户表控制层")
public class UserController {

    //注入UserService, 自动装配bean
    @Autowired
    private UserService userService;

    //添加用户
    @ApiOperation("添加一个用户")
    @PostMapping(value = "/addUser", produces = "application/json; charset=utf-8")
    public HashMap<String, Object> addUser(@RequestBody @Valid User user, BindingResult result){
        HashMap<String,Object> map = new HashMap<>();
        //如果表单传来的字段为空，则返回相应的提示信息
        if (result.hasErrors()){
            String message = result.getFieldError().getDefaultMessage();
            map.put("msg", message);
        }else {
            User user1 = userService.addUser(user);
            if (user1 != null) {
                map.put("msg","success");
            }else
                map.put("msg","error");
        }
        return map;
    }

    //删除用户
    @ApiOperation("删除用户")
    @PostMapping("/deleteUser")
    public HashMap<String, Object> deleteUser(@RequestParam(value = "uid") int uid){
        HashMap<String, Object> map = new HashMap<>();
        int i = userService.deleteUserById(uid);
        System.out.println(i);
        //判断是否删除成功
        if (i > 0)
            map.put("msg","success");
        else
            map.put("msg","error");
        return map;
    }

    //修改前，通过uid查询该用户信息
    @ApiOperation("通过uid查询用户信息")
    @PostMapping(value = "/updateBeforeQueryUser", produces = "application/json; charset=utf-8")
    public Optional<User> queryUserById(@RequestParam(value = "uid") int uid){
        Optional<User> user = userService.queryUserById(uid);
        return user;
    }

    //修改用户信息
    @ApiOperation("修改用户信息")
    @PostMapping(value = "/updateUserInfo", produces = "application/json; charset=utf-8")
    public HashMap<String, Object> updateUserInfo(@RequestBody @Valid User user, BindingResult result){
        HashMap<String, Object> map = new HashMap<>();
        //如果表单传来的字段为空，则返回相应的提示信息
        if (result.hasErrors()){
            String message = result.getFieldError().getDefaultMessage();
            map.put("msg",message);
        }
        else{
            User user1 = userService.updateUser(user);
            //判断是否修改成功
            if (user1 != null){
                map.put("msg","success");
            }else
                map.put("msg","error");
        }
        return map;
    }

    //查询所有用户并分页显示
    @ApiOperation("查询所有用户")
    @PostMapping(value = "/queryAllUserAndPage", produces = "application/json; charset=utf-8")
    public HashMap<String, Object> queryAllUserAndPage(@RequestParam(value = "currentPage") @ApiParam("当前页-每页五条数据") int currentPage){
        HashMap<String, Object> map = new HashMap<>();
//        //当前页如果小于等于0.则默认为第一页。
//        if (currentPage <= 0)
//            currentPage = 1;
        Page<User> userPage = userService.queryAllUser(currentPage);
        map.put("userList",userPage.getContent());           //当前页用户数量
        map.put("totalUser",userPage.getTotalElements());    //用户总数量
        map.put("pageSize",userPage.getSize());              //页面大小（每页能容纳的用户数量）
        map.put("totalPages",userPage.getTotalPages());      //总页数
        return map;
    }

    /*
    //通过用户名模糊查询并分页显示
    @RequestMapping("/chooseQueryUserAndPage")
    public HashMap<String, Object> chooseQueryUserAndPage(@RequestParam(value = "username") String username,@RequestParam(value = "currentPage") int currentPage){
        HashMap<String, Object> map = new HashMap<>();
        Page<User> userPage = userService.chooseQueryUser(username, currentPage);
        map.put("userList", userPage.getContent());         //当前页面用户数
        map.put("totalUser", userPage.getTotalElements());  //总用户数量
        map.put("pageSize", userPage.getSize());            //页面大小（每页能容纳的用户数量）
        map.put("totalPages", userPage.getTotalPages());    //总页数
        return map;
    }
    */

    //按条件(username 和 account)动态模糊查询并分页，当查询条件为空时，则查询全部
    @ApiOperation("通过用户名和账号的动态模糊查询")
    @PostMapping(value = "/dynamicQueryUserAndPage", produces = "application/json; charset=utf-8")
    public HashMap<String, Object> dynamicQueryUserAndPage(@RequestBody User user, @RequestParam(value = "currentPage") @ApiParam("当前页-每页五条数据") int currentPage){
        HashMap<String, Object> map = new HashMap<>();
        Page<User> userPage = userService.dynamicQuery(user, currentPage);
        //判断是否查到符合条件的用户，如果userPage.getContent().size() = 0，说明没有查到符合条件的用户
        if (userPage.getContent().size() > 0){
            map.put("userList",userPage.getContent());
            map.put("totalUser",userPage.getTotalElements());
            map.put("pageSize",userPage.getSize());
            map.put("totalPage",userPage.getTotalPages());
        }else
            map.put("msg","没有符合查询条件的用户");
        return map;

    }
}
