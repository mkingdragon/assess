package net.xstart.second.assess.controller.Item;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import io.swagger.annotations.ApiParam;
import net.xstart.second.assess.mapper.Item.ItemMapper;
import net.xstart.second.assess.pojo.Item.item;
import net.xstart.second.assess.service.Item.ItemService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

/**
 * @program: assess
 * @description: 项目的控制层
 * @author: 上河图
 * @create: 2020-10-11 09:37
 */

@RestController
@RequestMapping("/item")
@Api(tags = "项目表控制层")
public class ItemController{

    @Autowired
    private ItemMapper itemMapper;

    @Autowired
    private ItemService itemService;
    
/**
* @Description: 项目添加
* @Param: item:前端传入的数据封装成item类   BindingResult：校验前端传入的字段
* @return: Map<String, Object>
* @Author: 上河图
* @Date: 2020/10/12/10:41
*/
    //  @Valid  校验字段是否为空  @RequestBody的作用就是把json格式转换为java格式
    @ApiOperation("添加项目")
    @PostMapping(value = "/add", produces = "application/json; charset=utf-8")
    public Map<String, Object> AddItem(@RequestBody @Valid item item,BindingResult results) {
        Map<String, Object> map = new HashMap<>();
        item item1 = new item();
        item1.setProjectName(item.getProjectName()); //设置项目名字
        item1.setProjectDuration(item.getProjectDuration()); //设置项目时长
        item1.setProjectPrincipal(item.getProjectPrincipal()); //设置项目负责人
        item1.setProjectCreatTime(new Date()); //设置项目创建时间
        if (results.hasErrors()) {  //检测字段有无null异常
            map.put("status", "error"); //状态标识
            String message = results.getFieldError().getDefaultMessage();
            map.put("msg", message);  //封装错误信息

        }else {
            map.put("status", "success"); //状态标识
            itemMapper.save(item1);  //保存操作
        }

        return map;
    }

    /**
    * @Description: 测试 通过id删除对应的项目
    * @Param: id： 前端传入的参数
    * @return: Map<String, Object>
    * @Author: 上河图
    * @Date: 2020/10/12/11:24
    */
    @ApiOperation("通过id删除对应的项目")
    @Transactional
    @PostMapping(value = "/DeleteItem", produces = "application/json; charset=utf-8")
    public Map<String, Object> Delete_item(@RequestParam(value = "id") @ApiParam("项目id")int id){
        Map<String, Object> map = new HashMap<>();
        Boolean flag = itemService.DeleteItemById(id); //判断是否删除成功，返回的是Boolean类型
        if (flag){
            map.put("msg","删除成功");  //封装成功信息
        }else {
            map.put("status","error");  //标明错误的标志。
            map.put("msg","删除失败");  //封装错误信息
        }
        return map;
    }

    /**
    * @Description: 查看全部项目
    * @Param: null
    * @return: Map<String, Object>
    * @Author: 上河图
    * @Date: 2020/10/12/11:48
    */
    @ApiOperation("查看全部项目")
    @PostMapping(value = "/QueryItem", produces = "application/json; charset=utf-8")
    public Map<String, Object> QueryItem(){
        Map<String, Object> map = new HashMap<>();
        map.put("data",itemMapper.findAll());  //封装查找好的全部数据
        return map;
    }

    /**
    * @Description: 通过id查看对应的项目 然后更新的时候才能显示更新前的数据,为下一个更新做准备
    * @Param: id: 前端传入的参数
    * @return: Map<String, Object>
    * @Author: 上河图
    * @Date: 2020/10/12/11:55
    */
    @ApiOperation("通过id查看对应的项目")
    @PostMapping(value = "/QueryById", produces = "application/json; charset=utf-8")
    public  Optional<item> QueryById(@RequestParam(value = "id")@ApiParam("项目id") Integer id){

        System.out.println(itemMapper.findById(id));
        return itemMapper.findById(id);
    }


    /**
    * @Description: 更新项目
    * @Param: item :前端传入的数据封装成item
    * @return: java.util.Map<java.lang.String,java.lang.Object>
    * @Author: 上河图
    * @Date: 2020/10/12/12:22
    */
    @ApiOperation("更新项目")
    @PostMapping(value = "/UpdateItem", produces = "application/json; charset=utf-8")

    public Map<String, Object>  UpdateItem(@RequestBody  item item){

       Map<String, Object> map = new HashMap<>();
        try {
            //先查找出来，然后在进行覆盖
            Optional<item> item1 = itemMapper.findById(item.getId());
            item1.get().setProjectName(item.getProjectName()); //项目名称、
            item1.get().setProjectDuration(item.getProjectDuration()); //项目时长
            item1.get().setProjectPrincipal(item.getProjectPrincipal()); //项目负责人
            item1.get().setProjectUpdateTime(new Date());
             itemMapper.save(item1.get());
             map.put("success","更新成功");  //返回一个标志给前端
        }catch (Exception e){
            map.put("error","更新失败");     //返回一个标志给前端
        }

        return map;
    }
    
    
    /**
    * @Description: 通过项目名来模糊查询并进行分页
    * @Param: ProjectName：前端传入的项目名
    * @return: java.util.Map<java.lang.String,java.lang.Object>
    * @Author: 上河图
    * @Date: 2020/10/12/12:37
    */
    @ApiOperation("通过项目名来模糊查询并进行分页")
    @PostMapping(value = "/QueryLike", produces = "application/json; charset=utf-8")
    public Map<String,Object> Query_like(@RequestParam(value = "ProjectName",required = false) @ApiParam("项目名称") String ProjectName,
                                         @RequestParam(value = "currentPage") @ApiParam("当前页") int currentPage){

        System.out.println(ProjectName);
       Map<String, Object> map = new HashMap<>();
        //page只能输出分页后的容量，起始值，总页数，总条数
        Pageable page =  PageRequest.of(currentPage-1, 2);  //起始值是0 每一页最大容量为2
        if (!StringUtils.isEmpty(ProjectName)){  //如果项目名不为空，就按照项目名来模糊查询并进行分页
            Page<item> listMapper = itemService.QueryLikeAndLimit(ProjectName, page);
            List<item> listMapperContent = listMapper.getContent();  //转换为list类型才能输出分页好的内容
            map.put("data",listMapperContent);
            map.put("TotalElements",listMapper.getTotalElements());   //数据总条顺
            map.put("TotalPages",listMapper.getTotalPages());   //数据总页数

        }else {  //项目名为空，就查找全部并进行分页
            Page<item> items = itemMapper.findAll(page);
            List<item> list = items.getContent();
            map.put("data",list);
            map.put("TotalElements",items.getTotalElements()); //数据总条数
            map.put("TotalPages",items.getTotalPages());  // 数据总页数
        }

        return map;
    }
}
