package net.xstart.second.assess.pojo.Item;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @program: assess
 * @description: 项目表对应的实体类
 * @author: 上河图
 * @create: 2020-10-10 22:34
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "exam_project")
@ApiModel("项目实体类")
public class item {
/*
*        @Id:声明这是一个主键
         @GeneratedValue：主键生成策略 像自增策略
            strategy: 策略生成方式
         @Column: 对应数据库表中的字段
            name：字段的名称
* */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "项目id")
    private Integer id;

    // @NotBlank 用于String字段的校验
    @ApiModelProperty(value = "项目名")
    @NotBlank(message = "项目名不为空")
    @Column(name = "project_name")
    private String projectName;

    @NotBlank(message = "项目时长不为空")
    @Column(name = "project_duration")
    @ApiModelProperty(value = "项目时长")
    private String projectDuration;

    @NotBlank(message = "项目负责人不能为空")
    @Column(name = "project_principal")
    @ApiModelProperty(value = "负责人项目")
    private String projectPrincipal;


    @Column(name = "project_creattime")
    @ApiModelProperty(value = "项目创建时间")
    private Date projectCreatTime;



    @Column(name = "project_updatetime")
    @ApiModelProperty(value = "项目修改时间")
    private Date projectUpdateTime;


}
