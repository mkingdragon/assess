package net.xstart.second.assess.pojo.role;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @authoor Bilysen
 * @date 2020/10/17 - 9:51
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "role")
@ApiModel("角色实体类Role")
public class Role {
    @Id  //声明这是主键
    @GeneratedValue(strategy = GenerationType.IDENTITY)  //主键生成策略 如：自增--mysql 序列--oracle
    @Column(name = "role_id")  //对应数据库表中的字段名
    @ApiModelProperty("用户角色id")
    private Integer roleId;

    @Column(name = "role_name")
    @NotBlank(message = "角色名不为空")  // @NotBlank 用于String字段的校验
    @ApiModelProperty("角色名")
    private String roleName;

    @Column(name = "user_name")
    @NotBlank(message = "用户名不为空")
    @ApiModelProperty("用户名")
    private String userName;

    @Column(name = "create_date")
    @ApiModelProperty("用户角色创建日期,不需要传参，后台自动生成")
    private Date createDate;

    @Column(name = "modify_date")
    @ApiModelProperty("用户角色修改日期，不需要传参，后台自动生成")
    private Date modifyDate;

}
