package net.xstart.second.assess.pojo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @program: assess
 * @description: 用户表实体类
 * @author: WH
 * @create: 2020-10-10 12:46
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("用户表实体类")
@Entity
@Table(name = "user")
public class User {
    @ApiModelProperty("用户id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer uid;
    @ApiModelProperty("用户名")
    @NotBlank(message = "用户名不能为空")
    private String username;
    @ApiModelProperty("账号")
    @NotBlank(message = "账号不能为空")
    private String account;
    @ApiModelProperty("密码")
    @NotBlank(message = "密码不能为空")
    private String password;
    @ApiModelProperty("创建时间")
    private Date buildTime;
    @ApiModelProperty("修改时间")
    private Date modifyTime;

}
