package net.xstart.second.assess.pojo.work;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @description: 职位实体类
 * @Author Lin
 * @Date 2020/10/12 10:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("职位表实体类")
@Entity
@EntityListeners(value = AuditingEntityListener.class)
@Table(name = "work")
public class Work {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wid")
    @ApiModelProperty(value = "职位id")
    private Integer wid;

    @NotBlank(message = "职业不能为空")
    @Column(name = "work")
    @ApiModelProperty(value = "职位名")
    private String work;

    @NotNull(message = "在职时长不能为空")
    @Column(name = "work_time")
    @ApiModelProperty(value = "在职年限")
    private Integer workTime;

    @NotBlank(message = "用户名不能为空")
    @Column(name = "username")
    @ApiModelProperty(value = "用户名")
    private String username;

    @CreatedDate
    @Column(name = "build_time")
    @ApiModelProperty(value = "职位创建时间")
    private Date buildTime;

    @LastModifiedDate
    @Column(name = "modify_time")
    @ApiModelProperty(value = "职位修改时间")
    private Date modifyTime;

}
