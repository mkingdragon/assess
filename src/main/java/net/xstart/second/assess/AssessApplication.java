package net.xstart.second.assess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;



@SpringBootApplication
@EnableJpaAuditing  //开启审计功能

public class AssessApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssessApplication.class, args);
	}

}
