package net.xstart.second.assess.service.user;

import net.xstart.second.assess.pojo.user.User;
import org.springframework.data.domain.Page;
import java.util.Optional;

/**
 * @program: assess
 * @description: 业务逻辑处理层
 * @author: WH
 * @create: 2020-10-10 13:42
 */
public interface UserService {
    //1.添加一个用户
    User addUser( User user);

    //2.通过用户uid删除一个用户
    int deleteUserById(int uid);

    //3.通过uid查询用户
    Optional<User> queryUserById(int uid);

    //4.修改用户信息
    User updateUser(User user);

    //5.查询当前页的用户
    Page<User> queryAllUser(int currentPage);

    //6.按条件(模糊）查询用户并分页
//    Page<User> chooseQueryUser(String username, int currentPage);

    //7.按条件动态查询并分页
    Page<User> dynamicQuery(User user, int currentPage);

}
