package net.xstart.second.assess.service.Item;

import net.xstart.second.assess.mapper.Item.ItemMapper;
import net.xstart.second.assess.pojo.Item.item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @program: assess
 * @description: 项目业务逻辑层的实现
 * @author: 上河图
 * @create: 2020-10-11 09:35
 */

@Service
public class ItemServiceImpl implements ItemService{

    @Autowired
    private ItemMapper itemMapper;


//    通过id删除对应的项目
    @Override
    @Transactional
    public Boolean DeleteItemById(int id) {
        boolean flag = false;
        if (itemMapper.DeleteItemById(id)>0){
            flag = true;
        }
        return flag;
    }

    //分页加模糊查询
    @Override
    public Page<item> QueryLikeAndLimit(String name, Pageable pageable) {

        return itemMapper.QueryAndLimit(name,pageable);
    }


}
