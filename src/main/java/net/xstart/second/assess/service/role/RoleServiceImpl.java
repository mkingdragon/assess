package net.xstart.second.assess.service.role;

import net.xstart.second.assess.mapper.role.RoleMapper;
import net.xstart.second.assess.pojo.role.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @authoor Bilysen
 * @date 2020/10/17 - 15:38
 **/
@Service
public class RoleServiceImpl implements RoleService{
    @Autowired
    private RoleMapper roleMapper;

    //查询角色用户列表并进行分页
    @Override
    public Page<Role> queryRoles(int page) {
        //PageRequest对象是Pageable接口的实现类，可以通过静态方法of来
        //page：下标从0开始，因此我们生活中所说的第几页在这里需要减1 每页10条数据
        Pageable pageable= PageRequest.of(page-1,10);
        Page<Role> rolePage = roleMapper.findAll(pageable);
        return rolePage;
    }

    //通过id来查找角色用户
    @Override
    public Role queryRoleById(int id) {
        Role role = roleMapper.findById(id).get();
        return role;
    }

    //修改角色用户信息
    @Override
    public Role updateRole(Role role) {
        //先查询修改id的对象，并将Optional 类型通过get()转换为实体类型
        Role updateRole = roleMapper.findById(role.getRoleId()).get();
        //需要判断是否有修改role_name，如果没有修改则保持原样,修改则就修改为新的数据
        if(role.getRoleName()!=null){
            updateRole.setRoleName(role.getRoleName());
        }
        if (role.getUserName()!=null){
            updateRole.setUserName(role.getUserName());
        }
        updateRole.setModifyDate(new Date());
        Role changeRole = roleMapper.save(updateRole);
        return changeRole;
    }

    //添加角色用户
    @Override
    public Role addRole(Role role) {
        //添加用户，因此创建时间和修改时间需要自动生成
        role.setCreateDate(new Date());
        role.setModifyDate(new Date());
        Role addRole = roleMapper.save(role);
        return addRole;
    }

    //根据roleId删除角色用户
    @Override
    public int deleteRole(int id) {
        int i =roleMapper.deleteRoleById(id);
        return i;
    }

    //根据roleName或者userName进行模糊查询,并进行分页
    @Override
    public Page<Role> queryLike(String roleName,String userName, int page) {
        //分页参数 ,每页10条数据
        Pageable pageable= PageRequest.of(page-1,10);
        //复杂查询
        Specification<Role> spec = new Specification<Role>() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery query, CriteriaBuilder cb) {
                //第一种情况：客户没有输入的为空字符串默认全部查询
                Predicate res=null;
                //第二种情况：客户对roleName和userName都进行了模糊查询
                if(!StringUtils.isEmpty(roleName)&&!StringUtils.isEmpty(userName)){
                    Path rn = root.get("roleName");
                    Path un = root.get("userName");
                    Predicate l1 = cb.like(rn.as(String.class), "%" + roleName + "%");
                    Predicate l2 = cb.like(un.as(String.class), "%" + userName + "%");
                    res = cb.and(l1, l2);
                }else if(!StringUtils.isEmpty(roleName)){  //第三种情况:用户对roleName进行模糊查询
                    Path rn = root.get("roleName");
                    res = cb.like(rn.as(String.class), "%" + roleName + "%");
                }else if(!StringUtils.isEmpty(userName)){  //第四种情况:用户对userName进行模糊查询
                    Path un = root.get("userName");
                    res = cb.like(un.as(String.class), "%" + userName + "%");
                }
                return res;
            }
        };
        return roleMapper.findAll(spec,pageable);
    }
}
