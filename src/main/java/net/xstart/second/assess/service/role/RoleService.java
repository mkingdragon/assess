package net.xstart.second.assess.service.role;

import net.xstart.second.assess.pojo.role.Role;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @authoor Bilysen
 * @date 2020/10/17 - 15:41
 * role服务层
 **/
public interface RoleService {
    //查询所有角色用户,并进行分页
    public Page<Role> queryRoles(int page);

    //根据id查询相关角色用户
    public Role queryRoleById(int id);

    //根据id修改指定的角色用户
    public Role updateRole(Role role);

    //添加角色用户
    public Role addRole(Role role);

    //根据id删除角色用户
    public int deleteRole(int id);

    //根据roleName或者userName进行模糊查询,并进行分页
    public Page<Role> queryLike(String roleName,String userName,int page);
}
