package net.xstart.second.assess.service.user;

import net.xstart.second.assess.mapper.user.UserMapper;
import net.xstart.second.assess.pojo.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

/**
 * @program: assess
 * @description: 业务层实现类
 * @author: WH
 * @create: 2020-10-10 13:43
 */
@Service
public class UserServiceImpl implements UserService{

    //注入UserMapper, 自动装配bean
    @Autowired
    private UserMapper userMapper;

    //添加用户
    @Override
    public User addUser(User user) {
        //插入数据时，给时间设置默认值
        user.setBuildTime(new Date());
        user.setModifyTime(new Date());
        return userMapper.save(user);
    }

    //删除用户
    @Override
    public int deleteUserById(int uid) {
        return userMapper.deleteUserById(uid);
    }

    //通过uid查询用户
    @Override
    public Optional<User> queryUserById(int uid) {
        return userMapper.findById(uid);
    }

    //修改用户信息
    @Override
    public User updateUser(User user) {
        //findById(user.getUid()).orElse(null):通过uid查询用户，如果查不到这返回null
        User user1 = userMapper.findById(user.getUid()).orElse(null);
        if (user1 != null) {
            user1.setUsername(user.getUsername());
            user1.setAccount(user.getAccount());
            user1.setPassword(user.getPassword());
            user1.setModifyTime(new Date());
            return userMapper.save(user1);
        }
        return null;
    }

    //查询所有用户
    @Override
    public Page<User> queryAllUser(int currentPage) {
        //currentPage 当前页    page:索引从0开始; size:每页大小
        Pageable pageable = PageRequest.of(currentPage-1, 5);
        return userMapper.findAll(pageable);
    }

    /*
    //按条件(模糊）查询用户并分页
    @Override
    public Page<User> chooseQueryUser( String username, int currentPage) {
        Pageable pageable = PageRequest.of(currentPage-1, 5);
        return userMapper.findByUser(pageable, username);
    }
    */

    //按条件动态查询并分页
    @Override
    public Page<User> dynamicQuery(User user, int currentPage) {
        //分页
        Pageable pageable = PageRequest.of(currentPage-1, 5);

        return userMapper.findAll(new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                ArrayList<Predicate> list = new ArrayList<>();
                //如果用户名不为空，则拼接作为查询条件
                if (!StringUtils.isEmpty(user.getUsername())){
                    Predicate username = criteriaBuilder.like(root.get("username").as(String.class), "%"+user.getUsername()+"%");
                    list.add(username);
                }
                //如果账号不为空，则拼接作为查询条件
                if (!StringUtils.isEmpty(user.getAccount())){
                    Predicate account = criteriaBuilder.like(root.get("account").as(String.class), "%"+user.getAccount()+"%");
                    list.add(account);
                }
                return criteriaBuilder.and(list.toArray(new Predicate[]{}));
            }
        },pageable);
    }

}
