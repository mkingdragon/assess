package net.xstart.second.assess.service.work;

import net.xstart.second.assess.mapper.work.WorkMapper;
import net.xstart.second.assess.pojo.work.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;
import java.util.List;
import java.util.Optional;

/**
 * @description 职业业务层实现类
 * @Author Lin
 * @Date 2020/10/12 10:23
 */
@Service
public class WorkServiceImpl implements WorkService {

    @Autowired
    WorkMapper workMapper;

    //增加操作
    @Override
    public void addWork(Work work) {
        workMapper.save(work);
    }

    //删除操作
    @Override
    public int deleteWorkById(Integer id) {
        //workMapper.deleteById(id);
        int i = workMapper.deleteWorkById(id);
        return i;
    }

    //修改操作
    @Override
    public void updateWork(Work work) {
        Optional<Work> work1 = workMapper.findById(work.getWid());
        work1.get().setWork(work.getWork());
        work1.get().setWorkTime(work.getWorkTime());
        work1.get().setUsername(work.getUsername());
        workMapper.save(work1.get());
    }

    //查询操作：根据id查询职位
    @Override
    public Optional<Work> queryWorkById(Integer id) {
        Optional<Work> work = workMapper.findById(id);
        return work;
    }

    //查询操作：查询所有职位
    @Override
    public List<Work> queryAllWork() {
        List<Work> workList = workMapper.findAll();
        return workList;
    }

    //模糊查询并分页
    @Override
    public Page<Work> QueryAndLimit(String str) {
        Specification<Work> workSpecification = new Specification<Work>() {
            @Override
            public Predicate toPredicate(Root<Work> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Path<Object> path = root.get("work");
                Predicate like = criteriaBuilder.like(path.as(String.class), str);
                return like;
            }
        };
        PageRequest page = PageRequest.of(0, 2);
        Page<Work> works = workMapper.findAll(workSpecification, page);
        return works;
    }



    /*
    分页查询
    @Override
    public Page<Work> workPage(int page, int size) {
        PageRequest pageRequest = PageRequest.of(page, size);
        Page<Work> workPage = workMapper.findAll(pageRequest);
        return workPage;
    }

    模糊查询
    @Override
    public List<Work> queryWorkLike(String str) {
        Specification<Work> workSpecification = new Specification<Work>() {
            @Override
            public Predicate toPredicate(Root<Work> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Path<Object> path = root.get("work");
                Predicate like = criteriaBuilder.like(path.as(String.class), str);
                return like;
            }
        };
        List<Work> workList = workMapper.findAll(workSpecification, Sort.by(Sort.Direction.ASC, "wid"));
        return workList;
    }
    */
}
