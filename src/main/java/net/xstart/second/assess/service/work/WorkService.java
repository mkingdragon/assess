package net.xstart.second.assess.service.work;

import net.xstart.second.assess.pojo.work.Work;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * @description 职位业务层接口
 * @Author Lin
 * @Date 2020/10/11 19:00
 */

public interface WorkService {

    //增加操作
    void addWork(Work work);

    //删除操作
    int deleteWorkById(Integer id);

    //修改操作
    void updateWork(Work work);

    //查询操作：根据id查询职位
    Optional<Work> queryWorkById(Integer id);

    //查询操作：查询所有职位
    List<Work> queryAllWork();

    //模糊查询并分页
    Page<Work> QueryAndLimit(String str);

    /*
    分页查询
    Page<Work> workPage(int page, int size);

    模糊查询
    List<Work> queryWorkLike(String str);
    */
}
