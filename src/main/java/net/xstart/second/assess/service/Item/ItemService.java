package net.xstart.second.assess.service.Item;

import net.xstart.second.assess.pojo.Item.item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



/**
 * @program: assess
 * @description: 项目的业务逻辑层
 * @author: 上河图
 * @create: 2020-10-11 09:34
 */
public interface ItemService {



    //通过主键id删除项目
    Boolean DeleteItemById(int id);

    // //分页加模糊查询 前提是前端传入项目名
    Page<item> QueryLikeAndLimit(String name, Pageable pageable);


}
