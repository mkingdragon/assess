package net.xstart.second.assess.Item;

import net.xstart.second.assess.pojo.Item.item;
import net.xstart.second.assess.service.Item.ItemService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;


/**
 * @program: assess
 * @description: 业务逻辑层的测试
 * @author: 上河图
 * @create: 2020-10-11 17:30
 */

@SpringBootTest
public class itemServiceTest {
    @Autowired
    private ItemService itemService;

//测试项目删除
    @Test
    public void DeleteItem(){

        Boolean itemById = itemService.DeleteItemById(3);
        System.out.println(itemById);
    }

//    测试模糊查询加分页
    @Test
    public void QueryLikeAndLike(){

        Pageable page =  PageRequest.of(0, 3);  //page只能输出分页后的容量，起始值，总页数，总条数
        Page<item> list = itemService.QueryLikeAndLimit("系统",page);
        System.out.println(list);
    }

}
