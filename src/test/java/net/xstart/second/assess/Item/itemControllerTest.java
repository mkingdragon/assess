package net.xstart.second.assess.Item;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import net.minidev.json.JSONObject;


/**
 * @program: assess
 * @description: 控制层的测试类
 * @author: 上河图
 * @create: 2020-10-21 15:30
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class itemControllerTest {

    private MockMvc mockMvc; // 模拟MVC对象，

    @Autowired
    private WebApplicationContext wac; // 注入WebApplicationContext


    @Before // 在测试开始前初始化工作
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    //测试插入数据
    @Test
    public void AddItem() throws Exception {

        Map<String, Object> map = new HashMap<>();
        map.put("projectName","街道通行系统");    //项目名称
        map.put("projectDuration","300天");      //项目时长
        map.put("projectPrincipal","张无忌");      //项目负责人
//        要在Controller层中的方法参数加@RequestBody这个注解，不然就会一直报属性找不到，数据传不到Controller层
        MvcResult result = mockMvc.perform(post("/item/add").contentType(MediaType.APPLICATION_JSON_UTF8).content(JSONObject.toJSONString(map)))
                .andDo(print()) // 打印出请求和相应的内容
                .andExpect(status().isOk())// 模拟向testRest发送get请求
                .andReturn();// 返回执行请求的结果

        System.out.println(result.getResponse().getContentAsString());


    }

    //    测试删除对应id的项目
    @Test
    public void DeleteItem() throws Exception {
        MvcResult result = mockMvc.perform(post("/item/DeleteItem").param("id", "26"))
                .andDo(print()) // 打印出请求和相应的内容
                .andExpect(status().isOk())// 模拟向testRest发送get请求
                .andReturn();// 返回执行请求的结果

        System.out.println(result.getResponse().getContentAsString());

    }


    //查找全部项目内容
    @Test
    public void QueryItem() throws Exception {

        MvcResult result = mockMvc.perform(post("/item/QueryItem"))
                .andDo(print()) // 打印出请求和相应的内容
                .andExpect(status().isOk())// 模拟向testRest发送get请求
                .andReturn();// 返回执行请求的结果

        System.out.println(result.getResponse().getContentAsString());
    }

    //   测试通过id查看对应的项目
    @Test
    public void QueryItemById() throws Exception {
        MvcResult result = mockMvc.perform(post("/item/QueryById").param("id", "2"))
                .andDo(print()) // 打印出请求和相应的内容
                .andExpect(status().isOk())// 模拟向testRest发送get请求
                .andReturn();// 返回执行请求的结果

        System.out.println(result.getResponse().getContentAsString());

    }

    // 测试更新项目
    @Test
    public void UpdateItem() throws Exception {

        Map<String, Object> map = new HashMap<>();
        map.put("id",2);
        map.put("projectName","教师管理系统");
        map.put("projectDuration","30天");
        map.put("projectPrincipal","青兰");
        MvcResult result = mockMvc.perform(post("/item/UpdateItem").contentType(MediaType.APPLICATION_JSON_UTF8).content(JSONObject.toJSONString(map)))
                .andDo(print()) // 打印出请求和相应的内容
                .andExpect(status().isOk())// 模拟向testRest发送get请求
                .andReturn();// 返回执行请求的结果

        System.out.println(result.getResponse().getContentAsString());

    }

    //测试模糊查询加分页
    @Test
    public void QueryLikeAndLike() throws Exception {

        //这是有项目名和当前页

        MvcResult result1 = mockMvc.perform(post("/item/QueryLike").param("ProjectName","学生").param("currentPage","1"))
                .andDo(print()) // 打印出请求和相应的内容
                .andExpect(status().isOk())// 模拟向testRest发送get请求
                .andReturn();// 返回执行请求的结果
        System.out.println(result1.getResponse().getContentAsString());

        //只有当前页
        MvcResult result2 = mockMvc.perform(post("/item/QueryLike").param("currentPage","1"))
                .andDo(print()) // 打印出请求和相应的内容
                .andExpect(status().isOk())// 模拟向testRest发送get请求
                .andReturn();// 返回执行请求的结"

        System.out.println(result2.getResponse().getContentAsString());

    }
}
