package net.xstart.second.assess.work;

import net.xstart.second.assess.pojo.work.Work;
import net.xstart.second.assess.service.work.WorkService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

/**
 * @description Service层接口的测试
 * @Author Lin
 * @Date 2020/10/12 10:23
 */
@SpringBootTest
class WorkServiceTest {

	@Autowired
	WorkService workService;

	//增加操作
	@Test
	void addWork() {
		Work work = new Work();
		work.setWork("程序员");
		work.setWorkTime(3);
		workService.addWork(work);
	}

	//删除操作
	@Test
	void deleteWork(){
		workService.deleteWorkById(1);
	}

	//修改操作
	@Test
	void updateWork(){
		Work work = new Work();
		work.setWid(1);
		work.setWork("饿了么骑手");
		work.setWorkTime(8);
		workService.updateWork(work);
	}

	//查询操作：根据id查询职位
	@Test
	void queryWorkById(){
		Optional<Work> work = workService.queryWorkById(4);
		System.out.println(work);
	}

	//查询操作：查询所有职位
	@Test
	void queryAllWork(){
		List<Work> works = workService.queryAllWork();
		works.forEach(System.out::println);
	}

	//模糊查询并分页
	@Test
	void QueryAndLimit(){
		Page<Work> workPage = workService.QueryAndLimit("程%");
		List<Work> workList = workPage.getContent();
		workList.forEach(System.out::println);
	}


	/*
	//分页操作
	@Test
	void workPage(){
		Page<Work> page = workService.workPage(0, 2);
		System.out.println(page.getTotalPages());     //得到总页数
		System.out.println(page.getTotalElements());  //得到总条数
		System.out.println(page.getContent());        //得到数据集合列表
	}

	//模糊查询
	@Test
	void queryLike(){
		List<Work> workList = workService.queryWorkLike("程%");
		workList.forEach(System.out::println);
	}
	*/
}
