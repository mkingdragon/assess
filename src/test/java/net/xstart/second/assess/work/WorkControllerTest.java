package net.xstart.second.assess.work;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.xstart.second.assess.pojo.work.Work;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.StandardCharsets;

/**
 * @description
 * @Author Lin
 * @Date 2020/10/21 19:59
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class WorkControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mvc;

    @Before
    public void setupMockMvc() {
        mvc = MockMvcBuilders.webAppContextSetup(wac).build(); //初始化MockMvc对象
    }


    //添加操作
    @Test
    public void addWork() throws Exception{
        Work work = new Work();
        work.setWork("架构师");
        work.setWorkTime(3);
        work.setUsername("Lindom");
        ObjectMapper mapper = new ObjectMapper();
        String s = mapper.writeValueAsString(work);
        mvc.perform(MockMvcRequestBuilders.get("/work/add")
                .content(s) //传json参数
                .contentType(new MediaType("application", "json", StandardCharsets.UTF_8))
                .accept(new MediaType("application", "json", StandardCharsets.UTF_8))
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    //删除操作
    @Test
    public void deleteWork() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/work/delete")
                .param("id","6")
                .contentType(new MediaType("application", "json", StandardCharsets.UTF_8))
                .accept(new MediaType("application", "json", StandardCharsets.UTF_8))
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    //修改角色用户数据
    @Test
    public void updateRole() throws Exception {
        Work work = new Work();
        work.setWid(8);
        work.setWork("经理");
        work.setUsername("Lin");
        work.setWorkTime(15);
        ObjectMapper mapper = new ObjectMapper();
        String s = mapper.writeValueAsString(work);   //模拟前端表单传回的json字符串
        mvc.perform(MockMvcRequestBuilders.get("/work/update")
                .content(s)
                .contentType(new MediaType("application", "json", StandardCharsets.UTF_8))
                .accept(new MediaType("application", "json", StandardCharsets.UTF_8))
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    //查询操作：根据id查询职位
    @Test
    public void queryWorkById() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/work/queryById")
                .param("wid","7")
                .contentType(new MediaType("application", "json", StandardCharsets.UTF_8))
                .accept(new MediaType("application", "json", StandardCharsets.UTF_8))
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }


    //查询操作：查询所有职位
    @Test
    public void queryAll() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/work/queryAll")
                .contentType(new MediaType("application", "json", StandardCharsets.UTF_8))
                .accept(new MediaType("application", "json", StandardCharsets.UTF_8))
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    //模糊查询并分页
    @Test
    public void queryWorkLike() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/work/queryWorkLike")
                .param("str","%手")
                //.param("un","").param("cP","1")
                .contentType(new MediaType("application", "json", StandardCharsets.UTF_8))
                .accept(new MediaType("application", "json", StandardCharsets.UTF_8))
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

}
