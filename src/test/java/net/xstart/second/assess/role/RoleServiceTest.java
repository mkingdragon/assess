package net.xstart.second.assess.role;

import net.xstart.second.assess.mapper.role.RoleMapper;
import net.xstart.second.assess.pojo.Item.item;
import net.xstart.second.assess.pojo.role.Role;
import net.xstart.second.assess.service.role.RoleService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Optional;


/**
 * @authoor Bilysen
 * @date 2020/10/17 - 10:42
 * role层测试类
 **/
@SpringBootTest
public class RoleServiceTest {
    @Autowired
    RoleMapper roleMapper;
    @Autowired
    RoleService roleService;

    //查询所有用户
    @Test
    public void findRoles(){
        int page =1;   //初始化页面为第一页
        Page<Role> roles = roleService.queryRoles(page);
        int totalPages = roles.getTotalPages();//获得总页面
        long totalElements = roles.getTotalElements();   //获得总数据
        int pageElements = roles.getNumberOfElements();     //当前页面的数据数量
        List<Role> roleList = roles.getContent();  //获得当前页面的数据
        for (Role role : roleList) {
            System.out.println(role);
        }
        System.out.println("当前页面"+page);
        System.out.println("总页面"+totalPages);
        System.out.println("总数据"+totalElements);
        System.out.println("当前页面数据数量"+pageElements);
    }

    //根据id查询用户
    @Test
    public void findRoleById(){
        Role role = roleService.queryRoleById(4);
        System.out.println(role);
    }

    //添加用户
    @Test
    public void addRole(){
        Role role = new Role();
        role.setRoleName("普通用户");
        role.setUserName("紫曦");
        Role addRole = roleService.addRole(role);
        System.out.println("===msg:已添加用户:"+addRole.toString());
    }

    //修改用户
    @Test
    public void updateRole(){
        Role role = new Role();
        role.setRoleId(4);
        role.setRoleName("vip用户");
//        role.setUserName("麦克");
        Role role1 = roleService.updateRole(role);
        System.out.println("===msg:修改后的用户"+role1);
    }

    //删除用户
    @Test
    public void deleteRole(){
        int i = roleService.deleteRole(8);
        System.out.println("删除用户数："+i);
    }

    //模糊查询且分页
    @Test
    public void queryLikeTest(){
        String roleName = "vip";
        String userName = null;
        int page = 1;
        Page<Role> roles = roleService.queryLike(roleName,userName, page);
        int totalPages = roles.getTotalPages();//获得总页面
        long totalElements = roles.getTotalElements();   //获得总数据
        int pageElements = roles.getNumberOfElements();     //当前页面的数据数量
        List<Role> roleList = roles.getContent();  //获得当前页面的数据
        for (Role role1 : roleList) {
            System.out.println(role1);
        }
        System.out.println("当前页面"+page);
        System.out.println("总页面"+totalPages);
        System.out.println("总数据"+totalElements);
        System.out.println("当前页面数据数量"+pageElements);
    }

}
