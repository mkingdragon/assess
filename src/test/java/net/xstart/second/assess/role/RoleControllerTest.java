package net.xstart.second.assess.role;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.xstart.second.assess.pojo.role.Role;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.StandardCharsets;

/**
 * @authoor Bilysen
 * @date 2020/10/21 - 10:31
 * role接口的controller层测试类
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class RoleControllerTest {
    @Autowired
    private WebApplicationContext wac;

    private MockMvc mvc;

    @Before
    public void setupMockMvc(){
        mvc = MockMvcBuilders.webAppContextSetup(wac).build(); //初始化MockMvc对象
    }

    //查询全部的角色用户并分页显示
    @Test
    public void queryAllRoles() throws Exception {
        //mvc.perform() 执行请求  MockMvcRequestBuilders.post("")构造一个post请求
        mvc.perform(MockMvcRequestBuilders.post("/role/roleList")
                .param("cP","1")   //请求传递参数 这里只是当前页面的参数
                .contentType(new MediaType("application", "json", StandardCharsets.UTF_8)) //设置请求的内容类型为utf-8
                .accept(new MediaType("application", "json", StandardCharsets.UTF_8))  //设置接收端的内容类型为utf-8
        ).andExpect(MockMvcResultMatchers.status().isOk())  //看请求的码是否为200，否则报错
                .andDo(MockMvcResultHandlers.print());  //添加一个结果处理器，打印结果
    }


    //添加角色用户
    @Test
    public void addRole() throws Exception {
        Role role = new Role();
        role.setRoleName("普通用户");
        role.setUserName("锋神1");
        ObjectMapper mapper = new ObjectMapper();
        String s = mapper.writeValueAsString(role);   //模拟前端表单传回的json字符串
        mvc.perform(MockMvcRequestBuilders.post("/role/addRole")
                .content(s)    //设置主体为utf-8的json字符串
                .contentType(new MediaType("application", "json", StandardCharsets.UTF_8))
                .accept(new MediaType("application", "json", StandardCharsets.UTF_8))
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    //得到更新角色的数据
    @Test
    public void getUpdateList() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/role/getUpdateRole")
                .param("rid","1")
                .contentType(new MediaType("application", "json", StandardCharsets.UTF_8))
                .accept(new MediaType("application", "json", StandardCharsets.UTF_8))
        ).andExpect(MockMvcResultMatchers.status().isOk())
        .andDo(MockMvcResultHandlers.print());
    }

    //修改角色用户数据
    @Test
    public void updateRole() throws Exception {
        Role role = new Role();
        role.setRoleId(16);
        role.setRoleName("普通用户");
        role.setUserName("必成神");
        ObjectMapper mapper = new ObjectMapper();
        String s = mapper.writeValueAsString(role);   //模拟前端表单传回的json字符串
        mvc.perform(MockMvcRequestBuilders.post("/role/updateRole")
                .content(s)
                .contentType(new MediaType("application", "json", StandardCharsets.UTF_8))
                .accept(new MediaType("application", "json", StandardCharsets.UTF_8))
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }



    //删除角色用户
    @Test
    public void deleteRole() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/role/deleteRole")
                .param("rid","15")
                .contentType(new MediaType("application", "json", StandardCharsets.UTF_8))
                .accept(new MediaType("application", "json", StandardCharsets.UTF_8))
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    //模糊查询
    @Test
    public void queryByLike() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/role/queryByLike")
                .param("rn","普通").param("un","神").param("cP","1")
                .contentType(new MediaType("application", "json", StandardCharsets.UTF_8))
                .accept(new MediaType("application", "json", StandardCharsets.UTF_8))
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}
