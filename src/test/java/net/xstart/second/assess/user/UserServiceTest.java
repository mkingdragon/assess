package net.xstart.second.assess.user;

import net.minidev.json.JSONObject;
import net.xstart.second.assess.pojo.user.User;
import net.xstart.second.assess.service.user.UserService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @program: assess
 * @description: ServiceImpl测试类
 * @author: WH
 * @create: 2020-10-10 19:10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {


    private MockMvc mockMvc; // 模拟MVC对象，

    @Autowired
    private WebApplicationContext wac; // 注入WebApplicationContext


    @Before // 在测试开始前初始化工作
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

  //注入UserService，自动装配bean
    @Autowired
    private UserService userService;

    //添加用户测试
    @Test
    void addUser(){
        User user = new User();
        user.setUsername("小明");
        user.setAccount("xiaoming");
        user.setPassword("123456");
        user.setBuildTime(new Date());
        user.setModifyTime(new Date());
        User user1 = userService.addUser(user);
        System.out.println(user1);
    }

    //通过uid删除用户测试
    @Test
    void deleteUserById(){
        int i = userService.deleteUserById(2);
        System.out.println(i);
    }

    //通过uid查询用户测试
    @Test
    void queryUserById(){
        Optional<User> user = userService.queryUserById(1);
        System.out.println(user);
    }

    //修改用户信息测试
    @Test
    void updateUser(){
        User user = new User();
        user.setUid(1);
        user.setUsername("张三");
        user.setAccount("zhangsan");
        user.setPassword("654321");
        User user1 = userService.updateUser(user);
        System.out.println(user1);
    }



    //测试插入数据
    @org.junit.Test
    public void AddItem() throws Exception {


        Map<String, Object> map = new HashMap<>();
        map.put("projectName","街道通行系统");    //项目名称
        map.put("projectDuration","300天");      //项目时长
        map.put("projectPrincipal","张无忌");      //项目负责人
//        要在Controller层中的方法参数加@RequestBody这个注解，不然就会一直报400，数据传不到Controller层
        MvcResult result = mockMvc.perform(post("/item/add").contentType(MediaType.APPLICATION_JSON_UTF8).content(JSONObject.toJSONString(map)))
                .andDo(print()) // 打印出请求和相应的内容
                .andExpect(status().isOk())// 模拟向testRest发送get请求
                .andReturn();// 返回执行请求的结果

        System.out.println(result.getResponse().getContentAsString());


    }

    //查询所有用户测试
    @Test
    void queryAllUser(){
        Page<User> users = userService.queryAllUser(1);
        List<User> content = users.getContent();      //当前页面用户
        int size = users.getSize();                   //页面大小
        int totalPages = users.getTotalPages();       //总页面数
        long totalElements = users.getTotalElements();   //总记录数
        for (User user : content) {
            System.out.println(user);
        }
        System.out.println("页面大小："+ size );
        System.out.println("总页面数："+ totalPages);
        System.out.println("总记录数："+ totalElements);

    }

    /*
    //按条件模糊查询并分页显示测试
    @Test
    void chooseQueryUser(){
        Page<User> userPage = userService.chooseQueryUser("小", 1);
        for (User user : userPage.getContent()) {
            System.out.println(user);
        }
        System.out.println("总记录数："+userPage.getTotalElements());
        System.out.println("总页数："+userPage.getTotalPages());
        System.out.println("页面大小："+userPage.getSize());
    }
     */

    //按条件动态模糊查询并分页显示
    @Test
    void dynamicQuery(){
        User user = new User();
//        user.setUsername("小");
//        user.setAccount("xiaoh");
        Page<User> userPage = userService.dynamicQuery(user, 1);
        for (User user1 : userPage.getContent()) {
            System.out.println(user1);
        }
        System.out.println("总记录数:" + userPage.getTotalElements());
        System.out.println("页面总数："+ userPage.getTotalPages());
        System.out.println("页面大小："+ userPage.getSize());
    }

}
