package net.xstart.second.assess.user;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.xstart.second.assess.pojo.user.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.StandardCharsets;
import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {

    private MockMvc mockMvc; // 模拟MVC对象，

    @Autowired
    private WebApplicationContext wac; // 注入WebApplicationContext


    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();   // 在测试开始前初始化工作
    }

    //测试添加一个用户
    @Test
    @Transactional    //开启事务，回滚数据库数据
    public void addUserTest() throws Exception {
        User user = new User();
        user.setUsername("刘琦");
        user.setAccount("liuqi");
        user.setPassword("123456");
        mockMvc.perform(post("/user/addUser")   //发送post请求
                .content(JSON.toJSONString(user))          //将对象转成json字符串格式
                .contentType(MediaType.APPLICATION_JSON)   //数据格式
        )
                .andExpect(status().isOk())    //状态码为200
                .andDo(print());               //打印请求和相应的内容
    }

    //测试删除用户
    @Test
    @Transactional
    public void test() throws Exception {
        mockMvc.perform(post("/user/deleteUser")
                .param("uid", "1")
        )
                .andExpect(status().isOk())
                .andDo(print());
    }

    //测试通过uid查询用户信息
    @Test
    public void updateBeforeQueryUser() throws Exception {
        mockMvc.perform(post("/user/updateBeforeQueryUser")
                .param("uid", "1")
        )
                .andExpect(status().isOk())
                .andDo(print());
    }

    //测试修改用户信息
    @Test
    @Transactional
    public void updateUserInfo() throws Exception {
        User user = new User();
        user.setUid(18);
        user.setUsername("刘兰");
        user.setAccount("liulan");
        user.setPassword("123456");
        user.setModifyTime(new Date());
        mockMvc.perform(post("/user/updateUserInfo")
                .content(JSON.toJSONString(user))
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andDo(print());
    }

    //测试查询所有用户并分页显示
    @Test
    public void queryAllUserAndPage() throws Exception {
        mockMvc.perform(post("/user/queryAllUserAndPage")
                .param("currentPage", "1")
        )
                .andExpect(status().isOk())
                .andDo(print());
    }

    //测试按条件(username 和 account)动态模糊查询并分页，当查询条件为空时，则查询全部
    @Test
    public void dynamicQueryUserAndPage() throws Exception {
        User user = new User();
        user.setUsername("刘");
        user.setAccount("liu");
        mockMvc.perform(post("/user/dynamicQueryUserAndPage")
                .param("currentPage", "1")
                .content(JSON.toJSONString(user))
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andDo(print());
    }
}